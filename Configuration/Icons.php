<?php

return [
    'adminer-module' => [
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:t3adminer/Resources/Public/Icons/module-adminer.svg',
    ]
];
