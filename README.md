[![Latest Stable Version](https://poser.pugx.org/jigal/t3adminer/v/stable)](https://extensions.typo3.org/extension/t3adminer/)
[![TYPO3 11](https://img.shields.io/badge/TYPO3-11-orange.svg?style=flat-square)](https://get.typo3.org/version/11)
[![TYPO3 12](https://img.shields.io/badge/TYPO3-12-orange.svg?style=flat-square)](https://get.typo3.org/version/11)
[![Total Downloads](https://poser.pugx.org/jigal/t3adminer/d/total)](https://packagist.org/packages/jigal/t3adminer)
[![Monthly Downloads](https://poser.pugx.org/jigal/t3adminer/d/monthly)](https://packagist.org/packages/jigal/t3adminer)

# TYPO3 extension ``t3adminer``

Adminer as BE module to work with the database

|                  | URL                                              |
|------------------|--------------------------------------------------|
| **Repository:**  | https://gitlab.com/jigal/t3adminer               |
| **Read online:** | https://docs.typo3.org/p/t3adminer/main/en-us/   |
| **TER:**         | https://extensions.typo3.org/extension/t3adminer/ |
